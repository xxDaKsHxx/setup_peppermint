#!/bin/bash

CN="\n"

print() {
    echo -e "${CN}"$@"${CN}"
}

print_install() {
    echo -e "${CN}Installing "$@" ...${CN}"
}

run_as_root() {
    if [ $(id -u) != "0" ]; then
        clear
        print "Run as root ..."
        exit 1
    fi
}

check_peppermint() {
    if [ "$DESKTOP_SESSION" != "Peppermint" ]; then
        print "This script can only be executed in PeppermintOS ..."
        exit 1
    fi
}

arch_64() {
    if [ $(arch) != "x86_64" ]; then
        print "ARCH not supported ..."
        exit 1
    fi
}

usage() {
    cat <<"EOF"
__________                                                  .__           __   ________     _________
\______   \  ____  ______  ______    ____  _______   _____  |__|  ____  _/  |_ \_____  \   /   _____/
 |     ___/_/ __ \ \____ \ \____ \ _/ __ \ \_  __ \ /     \ |  | /    \ \   __\ /   |   \  \_____  \ 
 |    |    \  ___/ |  |_> >|  |_> >\  ___/  |  | \/|  Y Y  \|  ||   |  \ |  |  /    |    \ /        \
 |____|     \___  >|   __/ |   __/  \___  > |__|   |__|_|  /|__||___|  / |__|  \_______  //_______  /
                \/ |__|    |__|         \/               \/          \/                \/         \/ 


[-] This script is only available for PeppermintOS .
[+] Pressing "Ctrl + C" or "Ctrl + Z" may cause a hot reboot .
[*] This script should be used in TTY ( Ctrl+Alt+F[1-9] ) .

Made by @DAKX
EOF
    sleep 5
    clear
}

update() {
    print "Updating the repositories ..."
    apt-get update
}

dist_upgrade() {
    print "Dist-Upgrading the system ..."
    apt-get dist-upgrade
}

autoremove() {
    print "Autoremoving app dependencies ..."
    apt-get autoremove
}

remove() {
    apt-get --purge remove "$@"
}

install() {
    apt-get install "$@"
}

main() {
    run_as_root
    check_peppermint
    arch_64
    usage
    update
    print "Removing bloatwares ..."
    # Peppermint Bloat
    remove peppermint-browser-manager peppermint-cursor-resizer peppermint-thunderbird-themer peppermint-neofetch-switch peppermint-scrot-helper peppermint-firefox-themer peppermint-panel-reset snapd* flatpak* scrot* samba* dmz-cursor-theme htop* xfce4-appfinder* xfce4-xkb-plugin* xubuntu-icon-theme xfpanel-switch xfce4-screenshooter transmission* gdebi* mate-calc simple-scan* xviewer* mintinstall guvcview firefox* mintstick xreader* arandr* gnome-disk-utility* imagemagick* gnome-software* printer-driver* gnome-menus gnome-system-tools gnome-desktop3-data gnome-icon-theme gnome-themes* gnome-search-tool gnome-keyring gnome-accessibility-themes ubuntu-release-upgrader-core ubuntu-advantage-tools usb* xbitmaps nemo-dropbox xul-ext-ubufox xdg-desktop-portal* telnet system-config-printer* dconf-editor vim
    autoremove
    print_install "Required Apps ..."
    install xfce4-clipman-plugin xfce4-notes-plugin xfce4-netload-plugin xfce4-cpugraph-plugin xfce4-cpufreq-plugin xed breeze-cursor-theme nemo-terminal software-properties-common apt-transport-https curl p7zip* flameshot sct kazam alien gufw okteta tree vlc meld libreoffice lubuntu-restricted-extras bleachbit
    # DNS Settings
    cat <<"EOF"
    Add

    nameserver 8.8.8.8
    nameserver 8.8.4.4
EOF
    nano /etc/resolv.cof
    # Windows95 Theme
    echo 'deb http://download.opensuse.org/repositories/home:/bgstack15:/Chicago95/xUbuntu_18.04/ /' | sudo tee /etc/apt/sources.list.d/home:bgstack15:Chicago95.list
    curl -fsSL https://download.opensuse.org/repositories/home:bgstack15:Chicago95/xUbuntu_18.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home:bgstack15:Chicago95.gpg >/dev/null
    update
    install chicago95-theme-all
    print "Change to theme in Peppermint Settings and LXAppearance"
    # Kvantum for QT Apps
    sudo add-apt-repository ppa:papirus/papirus
    update
    install qt5-style-kvantum qt5-style-kvantum-themes
    # Shell
    print "Changing default shell ( bash ) to fish"
    install fish
    usermod --shell /bin/fish $USER
    # LXQT Runner
    install --no-install-recommends --no-install-suggests lxqt-runner
    print "Setup LXQT Runner as Alt+F2 from Settings"
    # Vivaldi
    wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | apt-key add -
    add-apt-repository 'deb https://repo.vivaldi.com/archive/deb/ stable main'
    update
    install vivaldi-stable
    # Wine
    dpkg --add-architecture i386
    update
    install wine32 wine-stable exe-thumbnailer
    # Flash Player
    sudo apt-add-repository multiverse
    update
    install flashplugin-installer
    # DockbarX
    add-apt-repository ppa:xuzhen666/dockbarx
    update
    install xfce4-dockbarx-plugin
    # Discord
    wget "https://discordapp.com/api/download?platform=linux&format=deb" -O "/tmp/discord.deb"
    dpkg -i /tmp/discord.deb
    apt install -f
    rm /tmp/discord.deb
    # Betterdiscord
    curl -O https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl
    chmod +x betterdiscordctl
    mv betterdiscordctl /usr/local/bin
    echo -e "${CN}To install BetterDiscord , open discord and type betterdiscordctl install${CN}"
    # AppImage Launcher
    curl -s https://api.github.com/repos/TheAssassin/AppImageLauncher/releases/latest | grep "browser_download_url.*bionic_amd64.deb" | cut -d : -f 2,3 | tr -d \" | wget -i -
    dpkg -i appimagelauncher_2.2.0-travis996.1a527bb.bionic_amd64.deb
    apt install -f
    rm appimagelauncher_2.2.0-travis996.1a527bb.bionic_amd64.deb
    # Emoji Picker
    curl -s https://api.github.com/repos/OzymandiasTheGreat/emoji-keyboard/releases/latest | grep "browser_download_url.*deb" | cut -d : -f 2,3 | tr -d \" | wget -i -
    dpkg -i python3-emoji-keyboard_2.2.0-1_all.deb
    apt install -f
    rm python3-emoji-keyboard_2.2.0-1_all.deb
    # Grub Customiser
    add-apt-repository ppa:danielrichter2007/grub-customizer
    update
    install grub-customizer
    # Xanmod Kernel
    echo 'deb http://deb.xanmod.org releases main' | tee /etc/apt/sources.list.d/xanmod-kernel.list && wget -qO - https://dl.xanmod.org/gpg.key | apt-key add -
    update
    install linux-xanmod
    # Mesa
    add-apt-repository ppa:kisak/kisak-mesa
    dist_upgrade
    # Remove the script
    rm setup_peppermint.sh

    echo -e "${CN}${CN}Rebooting ...${CN}"
    sleep 5
    reboot
}

main
