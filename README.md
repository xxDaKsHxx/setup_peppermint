# PeppermintOS Setup

[PeppermintOS](https://peppermintos.com/) is a [Lubuntu](https://lubuntu.me/) 18.04 based linux distro .

### This is a setup script for PeppermintOS

## Features

- Removes bloatware
- Removes snap and flatpak
- Install gufw (Firewall) , sct (Redshift alternate) , QT Apps
- Changes DNS to Google
- Install Windows 95 theme
- Install Flash Player
- Install Kvantum manager for QT Apps themeing over GTK
- Changes default to fish
- Install LXQT-Runner
- Install XFCE Official and Custom Plugins
- Install appimagelauncher for integrating appimages into the system
- Install emoji-picker
- Install latest xanmod-kernel and Mesa3D drivers